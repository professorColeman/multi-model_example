# Multi-Model Example
![](screenshot.png)
This example shows how to talk to multiple models in RunwayML at the same time and send results from one into the other. 
It uses the official ofxRunway addon and openFrameworks.