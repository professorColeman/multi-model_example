#pragma once

#include "ofMain.h"
#include "ofxRunway.h"

class ofApp : public ofBaseApp, public ofxRunwayListener {

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
    
    ofxRunway runwayMTG, runwayDream;
    ofImage mtgResult, dreamResult;
    vector<float> v;
    
    // Callback functions that process what Runway sends back
    void runwayInfoEvent(ofJson& info);
    void runwayErrorEvent(string& message);
		
};
