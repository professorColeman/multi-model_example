#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    // setup Runway
    runwayMTG.setup(this, "http://localhost:8000");
    runwayMTG.start();
    runwayDream.setup(this, "http://localhost:8001");
    runwayDream.start();
    
    //allocate out images
    mtgResult.allocate(1024, 1024, OF_IMAGE_COLOR);
    dreamResult.allocate(1024, 1024, OF_IMAGE_COLOR);
    
    //fill the vector with initial values
    for(int i = 0; i<512 ; i++){
        float f = ofRandom(-2.0, 2.0);
        v.push_back(f);
    }
}

//--------------------------------------------------------------
void ofApp::update(){

    //if the deep dreem model is ready
    if(!runwayDream.isBusy()){
        v.clear(); //empty the vector
        for(int i = 0; i<512 ; i++){ //generate a new random vector of floats
            float f = ofRandom(-3.0, 3.0);
            v.push_back(f);
        }
        //send the vector of floats
        ofxRunwayData data1;
        data1.setFloats("z",v);
        data1.setFloat("truncation", 0.1);
        runwayMTG.send(data1);
        //get the resulting images
        runwayMTG.get("image", mtgResult);
        //send the 1st result to the deep dream model
        ofxRunwayData data2;
        data2.setImage("image", mtgResult, OFX_RUNWAY_JPG);
        runwayDream.send(data2);
        
    }
    //get the deep dream result
    runwayDream.get("image", dreamResult);
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    if(dreamResult.isAllocated()){dreamResult.draw(0,0);}
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if(key == ' '){
        
        v.clear();
        for(int i = 0; i<512 ; i++){
            float f = ofRandom(-2.0, 2.0);
            v.push_back(f);
        }
    }
}

//--------------------------------------------------------------
// Runway sends information about the current model
//--------------------------------------------------------------
void ofApp::runwayInfoEvent(ofJson& info){
    ofLogNotice("ofApp::runwayInfoEvent") << info.dump(2);
}
// if anything goes wrong
//--------------------------------------------------------------
void ofApp::runwayErrorEvent(string& message){
    ofLogNotice("ofApp::runwayErrorEvent") << message;
}
//--------------------------------------------------------------

